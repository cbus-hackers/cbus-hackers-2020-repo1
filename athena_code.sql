


--drop table users;

CREATE table users AS 
SELECT
  "created_at"
, "id"
, "text"
, "in_reply_to_screen_name"
, "user_id"
, "user_id_str"
, "user_location"
, "user_url"
, "geo"
FROM
  twitter_feeds_vw;


CREATE OR REPLACE VIEW twitter_feeds_vw AS 
SELECT
  "created_at"
, "id"
, "id_str"
, "text"
, "display_text_range"
, "source"
, "truncated"
, "in_reply_to_status_id"
, "in_reply_to_status_id_str"
, "in_reply_to_user_id"
, "in_reply_to_user_id_str"
, "in_reply_to_screen_name"
, "user"."id" "user_id"
, "user"."id_str" "user_id_str"
, "user"."location" "user_location"
, "user"."url" "user_url"
, "geo"
FROM
  "db2-hackers".twitter_final;


